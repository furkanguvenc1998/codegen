use std::fmt::Display;

/// Defines a visibility for struct and field, function, module
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Visibility {
    /// pub
    Pub,
    /// pub(crate)
    PubCrate,
    /// pub(super)
    PubSuper,
}


impl Display for Visibility {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let str = match self {
            Visibility::Pub => String::from("pub"),
            Visibility::PubCrate => String::from("pub(crate)"),
            Visibility::PubSuper => String::from("pub(super)")
        };
        write!(f, "{}", str)
    }
}